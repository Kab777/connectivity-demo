# READ ME PLEASE!

This sample service uses `Python 3.8` along with `SQLite3` db

Please run `pip3.8 install -r requirements` to install two packages (requests
and xmltodict)

After that, please run
 
`python3.8 main.py {city} {checkin} {checkout}`
  
to start the app


To Run test cases:

`python3.8 test.py`

Note: It asks to make 2 HTTP POST requests in parallel, it's a bit ambiguous as 
I don't know if it's asking to write parallel in a non-blocking way or truly parallel
in a multi processing way. I chose the former one because the description isn't really clear. 


# Connectivity Demo
This is our task for potential new backend engineers. We expect this task to take less than a few hours of work

Please fork this repo and make a starting commit with the message 'start'.
Commit your finished code and push to your forked repo when you are finished. Thanks 😄

## TODO

Implement a simple services that fetches data from 2 APIs and combines and saves the data into a local database

**Step 1**

The service should read the following arguments from the command line
- City string
- Checkin string
- Checkout string

The 3 inputs will be string inputs. Do not worry about validation or anything else

**Step 2**

When the arguments are read from the command line, make **2 HTTP POST requests** in parallel to 'https://experimentation.getsnaptravel.com/interview/hotels' and 'https://experimentation.getsnaptravel.com/interview/legacy_hotels' respectively.

1) The first request will be in JSON to the endpoint 'https://experimentation.getsnaptravel.com/interview/hotels' with the following body:

```
{
  city : city_string_input,
  checkin : checkin_string_input,
  checkout : checkout_string_input,
  provider : 'snaptravel'
}
```

The responses will be in JSON and each response will have an array of hotels and the SnapTravel prices.
```
[{
  id : 12,
  hotel_name : 'Center Hilton',
  num_reviews : 209,
  address : '12 Wall Street, Very Large City',
  num_stars : 4,
  amenities : ['Wi-Fi', 'Parking'],
  image_url : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
  price : 132.11
},
...
]
```

2) The second request will be in XML to the endpoint 'https://experimentation.getsnaptravel.com/interview/legacy_hotels' with the following body:

```
<?xml version="1.0" encoding="UTF-8"?>
<root>
  <checkin>checkin_string_input</checkin>
  <checkout>checkout_string_input</checkout>
  <city>city_string_input</city>
  <provider>snaptravel</provider>
</root>
```

The responses will be in XML and each response will have an array of hotels and the retail prices.

```
<?xml version="1.0" encoding="UTF-8"?>
<root>
  <element>
    <id>12</id>
    <hotel_name>Center Hilton</hotel_name>
    <num_reviews>209</num_reviews>
    <address>12 Wall Street, Very Large City</address>
    <num_stars>4</num_stars>
    <amenities>
      <element>Wi-Fi</element>
      <element>Parking</element>
    </amenities>
    <image_url>https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg</image_url>
    <price>132.11</price>
  </element>
  ...
</root>
```

**Step 3**

After both these calls have returned take **only** the hotels that appear in both the responses and save the data in a local database. You can use any database you'd like (except for a flat file) and you can save the data in whatever format you wish (SQL, JSON, XML, etc). However, the data must be normalized from the 2 API calls (ie, you can't save half in JSON and half in XML format)

For example, if the first call returned hotels with id [10,12] with SnapTravel prices 192.34 and 112.33 and the second call returned hotels [12,13] with retail prices 132.11 and 321.62 respectively, you would only save hotel 12 in the database with a SnapTravel price of 112.33 and a retail price of 132.11

The following is an example of how you can save the data in a PostgreSQL database with JSONB fields:

```
id : 12,
hotel_name : 'Center Hilton',
num_reviews : 209,
address : '12 Wall Street, Very Large City',
num_stars : 4,
amenities : ['Wi-Fi', 'Parking'],
image_url : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
prices : {
  snaptravel: 112.33,
  'retail': 132.11
}
```

## Notes
* If you run into any technical difficulties contact aditi@snaptravel.com
* If you wish to write tests you can but it is not a strict requirement.