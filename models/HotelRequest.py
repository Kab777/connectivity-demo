import json
import xmltodict

class HotelRequest:
    def __init__(self, city, checkin, checkout, provider='snaptravel'):
        self.city = city
        self.checkin = checkin
        self.checkout = checkout
        self.provider = provider

    def to_json(self):
        return json.dumps(self.__dict__)

    def to_xml(self):
        return xmltodict.unparse({'root': self.__dict__})

    @classmethod
    def from_json(cls, data):
        return cls(**data)
