from models.Hotel import Hotel
import json
import xmltodict


class Hotels:
    def __init__(self, hotels):
        self.hotels = hotels

    def get_hotel_id_map(self):
        return {hotel.id: hotel for hotel in self.hotels}

    @classmethod
    def from_json(cls, json):
        hotels = list(map(Hotel.from_json, json['hotels']))
        return cls(hotels)

    @classmethod
    def from_xml(cls, xml):
        xmlDict = xmltodict.parse(xml, force_list={'element'})
        hotels = list(map(Hotel.from_xml, xmlDict['root']['element']))
        return cls(hotels)
