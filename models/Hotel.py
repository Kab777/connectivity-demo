class Hotel:
    def __init__(self, id, hotel_name, num_reviews, address, num_stars, amenities, image_url, price):
        self.id = int(id)
        self.hotel_name = str(hotel_name)
        self.num_reviews = int(num_reviews)
        self.address = str(address)
        self.numstars = int(num_stars)
        self.amenities = amenities
        self.image_url = str(image_url)
        self.price = float(price)

    @classmethod
    def from_json(cls, json):
        return cls(**json)

    @classmethod
    def from_xml(cls, xml):
        if 'element' in xml['amenities']:
            xml['amenities'] = xml['amenities']['element']
        return cls(**xml)

