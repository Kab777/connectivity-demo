import sys
import json

from STRequest import STRequest
from Database import Database
from models.HotelRequest import HotelRequest
from models.Hotels import Hotels


def get_common_hotels(jsonReponse, xmlResponse):
    jsonHotelMap = Hotels.from_json(jsonReponse).get_hotel_id_map()
    xmlHotelMap = Hotels.from_xml(xmlResponse).get_hotel_id_map()
    commonHotels = []
    for id, hotel in jsonHotelMap.items():
        if id in xmlHotelMap:
            hotel.price = {"snapTravel": hotel.price, "retail": xmlHotelMap[id].price}
            commonHotels.append(hotel)
    return commonHotels


def insert_hotels_into_db(db, hotels):
    for hotel in hotels:
        db.insert('Hotel', 'id, data', (str(hotel.id), json.dumps(hotel.__dict__)))


def get_hotels(db):
    return db.getTable('Hotel')


def main():
    city, checkin, checkout = sys.argv[1:]
    hotelRequest = HotelRequest(city, checkin, checkout)

    jsonEndpoint = "https://experimentation.getsnaptravel.com/interview/hotels"
    xmlEndpoint = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"

    db = Database('test.db')
    stRequest = STRequest()

    jsonResponse, xmlResponse = stRequest.get_hotel_responses(jsonEndpoint, xmlEndpoint, hotelRequest)
    commonHotels = get_common_hotels(jsonResponse.json(), xmlResponse.text)
    insert_hotels_into_db(db, commonHotels)
    print(get_hotels(db))


if __name__ == "__main__":
    main()
