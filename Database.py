import sqlite3


class Database:
    def __init__(self, db_name=None):
        self.conn = None
        self.cursor = None
        if db_name:
            self.open(db_name)

    def open(self, db_name):
        try:
            self.conn = sqlite3.connect(db_name)
            self.cursor = self.conn.cursor()
            sql_drop_hotel_table = """DROP TABLE IF EXISTS Hotel"""
            self.cursor.execute(sql_drop_hotel_table)

            sql_create_hotel_table = """ CREATE TABLE IF NOT EXISTS Hotel (
                                                id integer NOT NULL PRIMARY KEY,
                                                data json NOT NULL
                                            ); """
            self.cursor.execute(sql_create_hotel_table)

        except sqlite3.Error as e:
            print("DB connection error" + e)

    def close(self):
        if self.conn:
            self.conn.commit()
            self.cursor.close()
            self.conn.close()

    def __del__(self):
        self.close()

    def insert(self, table, column, data):
        self.cursor.execute("INSERT INTO {0} ({1}) VALUES (?, ?);".format(table, column), data)
        self.conn.commit()

    def getTable(self, table):
        query = "SELECT * from {0};".format(table)
        self.cursor.execute(query)
        rows = self.cursor.fetchall()
        return rows
