import asyncio
import requests


class STRequest:
    def __init__(self):
        self.eventloop = asyncio.get_event_loop()

    def post(self, url, data, headers):
        return requests.post(url, data=data, headers=headers)

    def get_hotel_responses(self, jsonEndpoint, xmlEndpoint, hotelRequest):
        return asyncio.get_event_loop().run_until_complete(
            self._get_async_hotel_responses(jsonEndpoint, xmlEndpoint, hotelRequest))

    async def _get_async_hotel_responses(self, jsonEndpoint, xmlEndpoint, hotelRequest):
        loop = asyncio.get_event_loop()

        jsonFuture = loop.run_in_executor(None, self.post, jsonEndpoint,
                                          hotelRequest.to_json(), self.getJsonHeader())
        xmlFuture = loop.run_in_executor(None, self.post, xmlEndpoint,
                                         hotelRequest.to_xml(), self.getXMLHeader())
        jsonResponse = await jsonFuture
        xmlResponse = await xmlFuture

        return jsonResponse, xmlResponse

    @classmethod
    def getXMLHeader(cls):
        return {'Content-Type': 'application/{0}; charset=utf-8'.format('xml')}

    @classmethod
    def getJsonHeader(cls):
        return {'Content-Type': 'application/{0}; charset=utf-8'.format('json')}
