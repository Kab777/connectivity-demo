import unittest
import main
from STRequest import STRequest
from models.HotelRequest import HotelRequest
from unittest import mock

SNAP_ENDPOINT = "https://experimentation.getsnaptravel.com/interview/hotels"
LEGACY_ENDPOINT = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"
SAMPLE_JSON_RESPONSE = {'hotels':
                            [{'id': 1, 'hotel_name': 'TrumpTower', 'num_reviews': 1, 'address': 'hell', 'num_stars': 2,
                              "amenities": ["Wi-Fi"],
                              'image_url': 'https://media.vanityfair.com/photos/5d502b54f5c6520008089c0b/16:9/w_2560%2Cc_limit/trump-epstein-clinton-conspiracy.jpg',
                              'price': 1000000},
                             {"id": 2, "hotel_name": "The Main Continental Hotel", "num_reviews": 12,
                              "address": "11 Dundas Street North, Very Large City", "num_stars": 5,
                              "amenities": ["Wi-Fi"],
                              "image_url": "https://images.trvl-media.com/hotels/2000000/1770000/1770000/1769973/a842d02c_b.jpg",
                              "price": 108.99}]}
SAMPLE_XML_RESPONSE = """<?xml version="1.0"?>
<root>
    <element>
        <id>1</id>
        <hotel_name>The Main Continental Hotel</hotel_name>
        <num_reviews>12</num_reviews>
        <address>11 Dundas Street North, Very Large City</address>
        <num_stars>5</num_stars>
        <amenities>
            <element>Wi-Fi</element>
        </amenities>
        <image_url>https://images.trvl-media.com/hotels/2000000/1770000/1770000/1769973/a842d02c_b.jpg</image_url>
        <price>102.99</price>
    </element>
    <element>
        <id>12</id>
        <hotel_name>Center Hilton</hotel_name>
        <num_reviews>209</num_reviews>
        <address>12 Wall Street, Very Large City</address>
        <num_stars>4</num_stars>
        <amenities>
            <element>Wi-Fi</element>
            <element>Parking</element>
        </amenities>
        <image_url>https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg</image_url>
        <price>199.99</price>
    </element>
</root>"""



def mocked_requests_post(*args, **kwargs):
    class MockJsonResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    if args[0] == SNAP_ENDPOINT:
        return MockJsonResponse(SAMPLE_JSON_RESPONSE, 200)
    elif args[0] == LEGACY_ENDPOINT:
        return MockJsonResponse(SAMPLE_XML_RESPONSE, 200)

    return MockJsonResponse(None, 404)


class ParallelRequestTest(unittest.TestCase):
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_fetch(self, mock_post):
        stRequest = STRequest()
        payload = HotelRequest("Toronto", "Today", "Tomorrow")

        jsonEndpoint = "https://experimentation.getsnaptravel.com/interview/hotels"
        xmlEndpoint = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"

        jsonResponse, xmlResponse = stRequest.get_hotel_responses(jsonEndpoint, xmlEndpoint, payload)
        self.assertIsNotNone(jsonResponse)
        self.assertIsNotNone(xmlResponse)
        self.assertEqual(len(mock_post.call_args_list), 2)

class GatherIntersectionTest(unittest.TestCase):
    def test_common_hotels(self):
        commonHotels = main.get_common_hotels(SAMPLE_JSON_RESPONSE, SAMPLE_XML_RESPONSE)
        self.assertEqual(len(commonHotels), 1)


# I should also write test cases for DB connections etc, but that's enough code for a code assignment..


if __name__ == '__main__':
    unittest.main()

